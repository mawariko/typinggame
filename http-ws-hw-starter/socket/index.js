import {
  User
} from "./user";
import {
  Room
} from "./room";

const rooms = [];
const userNames = [];

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    let currentRoom = null;
    let currentUser = null;

    if (userNames.includes(username)) {
      socket.emit('userPresent', username);
      return;
    } else {
      currentUser = new User(username, socket);
      userNames.push(username);
      socket.emit('renderRooms', rooms.map(room => ({
        name: room.name,
        usersCount: room.users.length,
        status: room.status
      })));
    }

    const removeUserFromRoom = () => {
      if (currentRoom) {
        currentRoom.removeUser(currentUser);
        if (!currentRoom.users.length) {
          currentRoom.dispose();
          const index = rooms.indexOf(currentRoom);
          if (index !== -1) {
            rooms.splice(index, 1);
          }
          io.emit('deleteRoom', currentRoom.name);
        }
        currentRoom = null;
      }
    };

    socket.on('create', (roomName) => {
      if (!roomName) {
        socket.emit('gameError', 'Invalid room name');
        return;
      }
      if (rooms.some(r => r.name === roomName)) {
        socket.emit('gameError', 'Room with this name already exits');
        return;
      }

      currentRoom = new Room(roomName, currentUser, io);
      rooms.push(currentRoom);
    });


    socket.on('creatorjoin', () => {
      if (currentRoom) {
        currentRoom.addUser(currentUser);
      }
    })

    socket.on('join', roomName => {
      const room = rooms.find(room => room.name === roomName);
      if (room && room.addUser(currentUser)) {
        currentRoom = room;
      }
      const statuses = currentRoom.users.map(u => ({
        userName: u.name,
        status: u.ready
      }));
      io.to(currentRoom.name).emit('statusUser', statuses)
    });

    socket.on('goToRooms', () => {
      removeUserFromRoom();
      socket.emit('leaveRoom', username);
    });

    socket.on('toggleStatusUser', () => {
      if (currentRoom) {
        currentUser.ready = !currentUser.ready;
        io.to(currentRoom.name).emit('statusUser', [{
          userName: currentUser.name,
          status: currentUser.ready
        }]);
        currentRoom.updateStatus();
      }
    });

    socket.on('progress', count => {
      const progressPercent = Math.floor(100 * count / currentRoom.text.length);

      currentUser.progress = count;
      currentUser.lastKeytime = new Date();
      io.to(currentRoom.name).emit('progressPercentUser', {
        userName: currentUser.name,
        progressPercent
      });
      currentRoom.checkPretermFinish();
    });

    socket.on('disconnect', () => {
      const index = userNames.indexOf(username);
      if (index !== -1) {
        userNames.splice(index, 1);
        removeUserFromRoom();
      }
    });
  })
};