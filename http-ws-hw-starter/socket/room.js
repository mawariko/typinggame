import * as config from "./config";
import { texts } from "../data";
import * as roomStatuses from "./room-statuses";

export class Room {
	constructor(name, creator, io) {
		this._io = io;
		this.name = name;
		this.users = [];
		this.status = roomStatuses.OPENED;
		this.textId = null;
		this.text = null;
		
		this.intervalId = null;
		this.secondsLeft = 0;

		this._io.emit('addCreatedRoom', this.name)
		creator.socket.emit('letcreatorin')
	}

	addUser(user) {
		let result = false;

		let usersCount = this.users.length;
		if (usersCount < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
			user.reset();

			this.users.push(user);
			usersCount++;
			result = true;

			user.socket.join(this.name);
			user.socket.emit('openRoom', this.name)
			user.socket.emit('enterRoom', this.users.map(u => u.name));
			user.socket.in(this.name).emit('enterRoom', [user.name])
			this._io.emit('numberOfUsers', {
				roomName: this.name,
				usersCount
			});
			if (usersCount === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
				this.status = roomStatuses.FULL;
				this._io.emit('statusRoom', {
					roomName: this.name,
					status: this.status
				});
			}
		} else {
			this._socket.emit('gameError', 'This room is full');
		}

		return result;
	}

	removeUser(user) {
		this.users = this.users.filter(u => u.name !== user.name);
		user.socket.leave(this.name);
		if (this.users.length) {
			this._io.emit('numberOfUsers', {
				roomName: this.name,
				usersCount: this.users.length
			});
			this._io.to(this.name).emit('leaveRoom', user.name);
			this.updateStatus();
			this.checkPretermFinish();
		}
	}

	updateStatus() {
		const oldStatus = this.status;
		if(this.status === roomStatuses.OPENED || this.status === roomStatuses.FULL) {
			const everyoneReady = this.users.every(u => u.ready);

			if(everyoneReady && this.users.length > 1) {
				this.initCountdown();
			} else {
				this.status = this.users.length < config.MAXIMUM_USERS_FOR_ONE_ROOM ? roomStatuses.OPENED : roomStatuses.FULL;
			}
		}

		if(this.status !== oldStatus) {
			this._io.emit('statusRoom', {
				roomName: this.name,
				status: this.status
			});
		}
	}

	initCountdown() {
		let textId;
		do {
			textId = Math.floor(Math.random() * texts.length);
		} while(textId === this.textId);

		this.status = roomStatuses.COUNTDOWN;
		this.textId = textId;
		this.text = texts[textId];
		this.secondsLeft = config.SECONDS_TIMER_BEFORE_START_GAME;
		this._io.to(this.name).emit('startCountdown', { secondsLeft: this.secondsLeft, textId });
		this.intervalId = setInterval(this.countdownHandler.bind(this), 1000);
	}

	initGame() {
		this.status = roomStatuses.RUNNING;
		this.secondsLeft = config.SECONDS_FOR_GAME;
		this._io.to(this.name).emit('startGame', { secondsLeft: this.secondsLeft });
		this.intervalId = setInterval(this.gameHandler.bind(this), 1000);
	}

	finishGame() {
		clearInterval(this.intervalId);

		const list = this.users
			.sort((a, b) => {
				const progressDiff = b.progress - a.progress;
				return progressDiff !== 0 ? progressDiff : a.lastKeytime - b.lastKeytime;
			})
			.map(u => u.name);

		this.status = this.users.length < config.MAXIMUM_USERS_FOR_ONE_ROOM ? roomStatuses.OPENED : roomStatuses.FULL;
			
		this._io.to(this.name).emit('finishGame', list);
		this._io.emit('statusRoom', {
			roomName: this.name,
			status: this.status
		});
		this.users.forEach(u => u.reset());
	}

	checkPretermFinish() {
		if (this.text) {
			if (this.users.every(u => u.progress == this.text.length)) {
				this.finishGame();
			}
		}
	}

	countdownHandler() {
		this.secondsLeft--;
		if(this.secondsLeft > 0) {
			this._io.to(this.name).emit('countdown', this.secondsLeft);
		} else {
			clearInterval(this.intervalId);
			this.initGame();
		}
	}

	gameHandler() {
		this.secondsLeft--;
		if(this.secondsLeft > 0) {
			this._io.to(this.name).emit('tickGame', this.secondsLeft);
		} else {
			this.finishGame();
		}
	}

	dispose() {
		if (this.intervalId) {
			clearInterval(this.intervalId);
		}
	}
}